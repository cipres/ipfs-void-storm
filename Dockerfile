FROM voidlinux/voidlinux

RUN xbps-install -Syu xbps
RUN xbps-install -Su -y
RUN xbps-install -y bash brig gx
RUN xbps-install -y \
		python3 python3-devel python3-pip \
		curl wget git automake autoconf make \
		yq \
		openssh \
		python3-aiohttp \
		python3-aiohttp_socks

# void still uses the old go-ipfs, so pull kubo 0.17.0 manually ..
RUN wget https://dist.ipfs.tech/kubo/v0.17.0/kubo_v0.17.0_linux-amd64.tar.gz && \
	tar -xvf kubo_v0.17.0_linux-amd64.tar.gz && cp kubo/ipfs /usr/bin && \
	rm kubo_v0.17.0_linux-amd64.tar.gz

RUN git clone https://gitlab.com/cipres/ipfs-ci-tools && \
	cd ipfs-ci-tools && make install

RUN mkdir -p /etc/go-ipfs
COPY ipfs-peering-pinata.json /etc/go-ipfs

CMD ipfs init

ENTRYPOINT []
